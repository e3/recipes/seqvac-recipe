require seqvac,master
require sequencer,2.2.7
require iocStats,3.1.16

#Set-up environment variables
epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "MPSVacSeq")

#Loading DBs
#dbLoadTemplate("$(TOP)/template/mpsvac-plc-ess.substitutions", "PREFIX=MPSoS-MPSVac:,SUBSCRIPT=$(SUBSCRIPT-MPSVac),REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")
dbLoadTemplate("$(TOP)/seqvac-loc/seqvacApp/Db/mpsvac-seq.substitutions", "IOCNAME=$(IOCNAME)")
dbLoadRecords("dbExample1.db","user=manuelzaerasanz")

#Include IOC stats for monitoring of our IOC
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

iocInit()

seq("sncExample","user=manuelzaerasanz")

dbl > "$(TOP)/$(IOCNAME)_PVs.list"

