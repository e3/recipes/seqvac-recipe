require seqvac,master
require sequencer,2.2.7
require iocStats,3.1.16

#Set-up environment variables
epicsEnvSet(TOP, "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "MPSVacSeq")

#Loading DBs
#dbLoadTemplate("$(TOP)/template/mpsvac-plc-ess.substitutions", "PREFIX=MPSoS-MPSVac:,SUBSCRIPT=$(SUBSCRIPT-MPSVac),REC=Ctrl-IOC-001:,IOCNAME=$(IOCNAME)")
dbLoadTemplate("$(TOP)/seqvac-loc/seqvacApp/Db/mpsvac-seq.substitutions", "IOCNAME=$(IOCNAME)")
#dbLoadRecords("dbExample1.db","user=manuelzaerasanz")

#Include IOC stats for monitoring of our IOC
iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

iocInit()

#seq("sncExample","user=manuelzaerasanz")
seq("sncMPSVac", "system=MPSoS-MPSVac:Ctrl-IOC-002:MPSVacSeq-,V0=LEBT-010:Vac-VVS-20000:,V1=LEBT-010:Vac-VVS-40000:,V2=RFQ-010:Vac-VVS-10000:,V3=DTL-010:Vac-VVS-10000:,V4=DTL-020:Vac-VVS-10000:,V5=DTL-030:Vac-VVS-10000:,V6=DTL-040:Vac-VVS-10000:,V7=DTL-050:Vac-VVS-10000:")

dbl > "$(TOP)/$(IOCNAME)_PVs.list"

