program sncMPSVac
/*Compilation options*/
option +r; /*Make re-entrant*/
option +s; /*Make safe*/

/*Variables*/
#define N_VVS 8 //Number of vacuuum sector gate valves

short StartVVSTest,EndVVSTest,InformFBIS,InformMPSVac,InformVac,TestGo; //OPI info
double TimeEval,TimeStep; //OPI info
short InitState,EndState,ReqOpenSet,EvalOpenSet,ReqCloseReset,EvalCloseReset,EvalPassed,EvalFailed,EvalTest; //States of the sequencer
short OpOpen,OpClose,OpSetVBP,OpResetVBP; //Operations to be performed by the sequencer

short VVS_Sw_C_En[N_VVS]; //Simulated: Switches cable connected or not  
short VVS_Sw_C_Stat[N_VVS]; //Simulated: Channel in the PLC modules valid or not
short VVS_O_En[N_VVS]; //Simulated: Valve fully open or not
short VVS_C_En[N_VVS]; //Simulated: Valve fully closed or not
short VVS_OC_Err[N_VVS]; //Simulated: Any error or discrepancy in position switches for valves
short VVS_BP_C_En[N_VVS]; //Simulated: VBP cable connected or not
short VVS_BP_C_Stat[N_VVS]; //Simulated: VBP cable channel in PLC modules valid or not
short VVS_BP_Err[N_VVS]; //Simulated: VBP error
short VVS_BP_En[N_VVS]; //Simulated: VBP set or reset
short VVS_BP_Stat[N_VVS]; //Simulated: VBP channel status
short VVS_OR[N_VVS]; // Simulated: Feedback from vacuum controls about VVS fully opened
short VVS_CR[N_VVS]; //Simulated: Feedback from vacuum controls about VVS fully closed
short VVS_SBP[N_VVS]; //Simulated: Feedback from vacuum controls about VVS Set VBP
short VVS_RBP[N_VVS]; //Simulated: Feedback from vacuum controls about VVS Reset VBP

short VVS_CB_T[N_VVS]; //Sequencer: Cable test for position switches 
short VVS_O_T[N_VVS]; //Sequencer: Open test for position switches
short VVS_C_T[N_VVS]; //Sequencer: Close test for position switches
short VVS_BP_CB_T[N_VVS]; //Sequencer: Cable test for VBP
short VVS_BP_S_T[N_VVS]; //Sequencer: VBP Set test
short VVS_BP_R_T[N_VVS]; //Sequencer: VBP Reset test
short VVS_SEL[N_VVS]; //Sequencer: VVS selected or not for the test
short VVS_SEQ_CMD_O[N_VVS]; //Sequencer: Command to Open valve to Vac.Controls
short VVS_SEQ_CMD_C[N_VVS]; //Sequencer: Command to Close valve to Vac.Controls
short VVS_SEQ_CMD_S_BP[N_VVS]; //Sequencer: Command to Set VBP to Vac. Controls
short VVS_SEQ_CMD_R_BP[N_VVS]; //Sequencer: Command to Reset VBP to Vac. Controls

unsigned int i; //Used in loops

//OPIs PVs
assign StartVVSTest to "{system}CmdStartSeqVVS"; //Button in the OPI to start test
assign EndVVSTest to "{system}CmdStopSeqVVS"; // Button in the OPI to stop test
assign TestGo to "{system}TestOnVVS"; //Flag in OPI and PV indicating if the test is on-going or not
assign InformFBIS to "{system}CmdTestFBIS"; //Flag in OPI and PV of the sequencer informing FBIS about test initiated 
assign InformVac to "{system}CmdTestVac"; //Flag in OPI and PV of the sequencer informing Vacuum Controls about Test initiated
assign InformMPSVac to "{system}CmdTest"; //Flag in OPI and PV of the sequencer informing MPSVac about test initiated
assign InitState to "{system}TestModeInitVVS"; //Flag in OPI: Informing start test_state in the sequencer
assign EndState to "{system}TestModeEndVVS"; //Flag in OPI: Informing stop test_state in the sequencer
assign ReqOpenSet to "{system}TestReqOpenVVS"; //Flag in OPI: Request to vacuum controls to open and set VBPs of the selected valves
assign ReqCloseReset to "{system}TestReqCloseVVS"; //Flag in OPI: Request to vacuum controls to close and reset VBPs of the selected valves
assign EvalOpenSet to "{system}TestEvalOpenVVS"; //Flag in OPI: Evaluate signals after opening/setting VBPs of the selected valves
assign EvalCloseReset to "{system}TestEvalCloseVVS"; //Flag in OPI: Evaluate signals after closing/resetting VBPs of the selected valves
assign EvalTest to "{system}EvalTestVVS"; //Flag in OPI: Final evaluation
assign TimeEval to "{system}SeqSetTimeEvalVVS"; //Value in OPI and PV: Time from the request to vaccum controls to the evaluation done by the sequencer
assign TimeStep to "{system}SeqSetTimeStepVVS"; //Value in OPI and PV: Time between steps of the sequencer (except for evaluation)
assign OpOpen to "{system}SelOpenVVS"; //Flag in OPI and PV: Open VVS operation chosen for the test
assign OpClose to "{system}SelCloseVVS"; //Flag in OPI and PV: Close VVS operation chosen for the test
assign OpSetVBP to "{system}SelSetVBPVVS"; //Flag in OPI and PV: Set VBP operation chosen for the test
assign OpResetVBP to "{system}SelResetVBPVVS"; //Flag in OPI and PV: Reset VBP operation chosen for the test

//SIMULATION PVs
assign VVS_Sw_C_En to {"{V0}MPSVacSim-SwitCableEn","{V1}MPSVacSim-SwitCableEn","{V2}MPSVacSim-SwitCableEn","{V3}MPSVacSim-SwitCableEn","{V4}MPSVacSim-SwitCableEn",
					"{V5}MPSVacSim-SwitCableEn", "{V6}MPSVacSim-SwitCableEn", "{V7}MPSVacSim-SwitCableEn"};
assign VVS_Sw_C_Stat to {"{V0}MPSVacSim-SwitCableStat","{V1}MPSVacSim-SwitCableStat","{V2}MPSVacSim-SwitCableStat","{V3}MPSVacSim-SwitCableStat","{V4}MPSVacSim-SwitCableStat",
					"{V5}MPSVacSim-SwitCableStat", "{V6}MPSVacSim-SwitCableStat", "{V7}MPSVacSim-SwitCableStat"};
assign VVS_O_En to {"{V0}MPSVacSim-OpenEn","{V1}MPSVacSim-OpenEn","{V2}MPSVacSim-OpenEn","{V3}MPSVacSim-OpenEn","{V4}MPSVacSim-OpenEn",
					"{V5}MPSVacSim-OpenEn", "{V6}MPSVacSim-OpenEn", "{V7}MPSVacSim-OpenEn"};
assign VVS_C_En to {"{V0}MPSVacSim-CloseEn","{V1}MPSVacSim-CloseEn","{V2}MPSVacSim-CloseEn","{V3}MPSVacSim-CloseEn","{V4}MPSVacSim-CloseEn",
					"{V5}MPSVacSim-CloseEn", "{V6}MPSVacSim-CloseEn", "{V7}MPSVacSim-CloseEn"};
assign VVS_OC_Err to {"{V0}MPSVacSim-OpenCloseErr","{V1}MPSVacSim-OpenCloseErr","{V2}MPSVacSim-OpenCloseErr","{V3}MPSVacSim-OpenCloseErr","{V4}MPSVacSim-OpenCloseErr",
					"{V5}MPSVacSim-OpenCloseErr", "{V6}MPSVacSim-OpenCloseErr", "{V7}MPSVacSim-OpenCloseErr"};
assign VVS_BP_C_En to {"{V0}MPSVacSim-BPCableEn","{V1}MPSVacSim-BPCableEn","{V2}MPSVacSim-BPCableEn","{V3}MPSVacSim-BPCableEn","{V4}MPSVacSim-BPCableEn",
					"{V5}MPSVacSim-BPCableEn", "{V6}MPSVacSim-BPCableEn", "{V7}MPSVacSim-BPCableEn"};
assign VVS_BP_C_Stat to {"{V0}MPSVacSim-BPCableStat","{V1}MPSVacSim-BPCableStat","{V2}MPSVacSim-BPCableStat","{V3}MPSVacSim-BPCableStat","{V4}MPSVacSim-BPCableStat",
					"{V5}MPSVacSim-BPCableStat", "{V6}MPSVacSim-BPCableStat", "{V7}MPSVacSim-BPCableStat"};
assign VVS_BP_Err to {"{V0}MPSVacSim-BPErr","{V1}MPSVacSim-BPErr","{V2}MPSVacSim-BPErr","{V3}MPSVacSim-BPErr","{V4}MPSVacSim-BPErr",
					"{V5}MPSVacSim-BPErr", "{V6}MPSVacSim-BPErr", "{V7}MPSVacSim-BPErr"};
assign VVS_BP_En to {"{V0}MPSVacSim-BPEn","{V1}MPSVacSim-BPEn","{V2}MPSVacSim-BPEn","{V3}MPSVacSim-BPEn","{V4}MPSVacSim-BPEn",
					"{V5}MPSVacSim-BPEn", "{V6}MPSVacSim-BPEn", "{V7}MPSVacSim-BPEn"};
assign VVS_BP_Stat to {"{V0}MPSVacSim-BPStat","{V1}MPSVacSim-BPStat","{V2}MPSVacSim-BPStat","{V3}MPSVacSim-BPStat","{V4}MPSVacSim-BPStat",
					"{V5}MPSVacSim-BPStat", "{V6}MPSVacSim-BPStat", "{V7}MPSVacSim-BPStat"};
assign VVS_OR to {"{V0}VacSeqSim-OpenedR","{V1}VacSeqSim-OpenedR","{V2}VacSeqSim-OpenedR","{V3}VacSeqSim-OpenedR","{V4}VacSeqSim-OpenedR",
					"{V5}VacSeqSim-OpenedR", "{V6}VacSeqSim-OpenedR", "{V7}VacSeqSim-OpenedR"};
assign VVS_CR to {"{V0}VacSeqSim-ClosedR","{V1}VacSeqSim-ClosedR","{V2}VacSeqSim-ClosedR","{V3}VacSeqSim-ClosedR","{V4}VacSeqSim-ClosedR",
					"{V5}VacSeqSim-ClosedR", "{V6}VacSeqSim-ClosedR", "{V7}VacSeqSim-ClosedR"};
assign VVS_SBP to {"{V0}VacSeqSim-SetBPR","{V1}VacSeqSim-SetBPR","{V2}VacSeqSim-SetBPR","{V3}VacSeqSim-SetBPR","{V4}VacSeqSim-SetBPR",
					"{V5}VacSeqSim-SetBPR", "{V6}VacSeqSim-SetBPR", "{V7}VacSeqSim-SetBPR"};
assign VVS_RBP to {"{V0}VacSeqSim-ResetBPR","{V1}VacSeqSim-ResetBPR","{V2}VacSeqSim-ResetBPR","{V3}VacSeqSim-ResetBPR","{V4}VacSeqSim-ResetBPR",
					"{V5}VacSeqSim-ResetBPR", "{V6}VacSeqSim-ResetBPR", "{V7}VacSeqSim-ResetBPR"};

//SEQUENCER PVs
assign VVS_CB_T to {"{V0}MPSVacSeq-CableTest","{V1}MPSVacSeq-CableTest","{V2}MPSVacSeq-CableTest","{V3}MPSVacSeq-CableTest","{V4}MPSVacSeq-CableTest",
					"{V5}MPSVacSeq-CableTest", "{V6}MPSVacSeq-CableTest", "{V7}MPSVacSeq-CableTest"};
assign VVS_O_T to {"{V0}MPSVacSeq-OpenTest","{V1}MPSVacSeq-OpenTest","{V2}MPSVacSeq-OpenTest","{V3}MPSVacSeq-OpenTest","{V4}MPSVacSeq-OpenTest",
					"{V5}MPSVacSeq-OpenTest", "{V6}MPSVacSeq-OpenTest", "{V7}MPSVacSeq-OpenTest"};
assign VVS_C_T to {"{V0}MPSVacSeq-CloseTest","{V1}MPSVacSeq-CloseTest","{V2}MPSVacSeq-CloseTest","{V3}MPSVacSeq-CloseTest","{V4}MPSVacSeq-CloseTest",
					"{V5}MPSVacSeq-CloseTest", "{V6}MPSVacSeq-CloseTest", "{V7}MPSVacSeq-CloseTest"};
assign VVS_BP_CB_T to {"{V0}MPSVacSeq-BPCableTest","{V1}MPSVacSeq-BPCableTest","{V2}MPSVacSeq-BPCableTest","{V3}MPSVacSeq-BPCableTest","{V4}MPSVacSeq-BPCableTest",
					"{V5}MPSVacSeq-BPCableTest", "{V6}MPSVacSeq-BPCableTest", "{V7}MPSVacSeq-BPCableTest"};
assign VVS_BP_S_T to {"{V0}MPSVacSeq-BPSetTest","{V1}MPSVacSeq-BPSetTest","{V2}MPSVacSeq-BPSetTest","{V3}MPSVacSeq-BPSetTest","{V4}MPSVacSeq-BPSetTest",
					"{V5}MPSVacSeq-BPSetTest", "{V6}MPSVacSeq-BPSetTest", "{V7}MPSVacSeq-BPSetTest"};
assign VVS_BP_R_T to {"{V0}MPSVacSeq-BPResetTest","{V1}MPSVacSeq-BPResetTest","{V2}MPSVacSeq-BPResetTest","{V3}MPSVacSeq-BPResetTest","{V4}MPSVacSeq-BPResetTest",
					"{V5}MPSVacSeq-BPResetTest", "{V6}MPSVacSeq-BPResetTest", "{V7}MPSVacSeq-BPResetTest"};
assign VVS_SEL to {"{V0}MPSVacSeq-TestSel","{V1}MPSVacSeq-TestSel","{V2}MPSVacSeq-TestSel","{V3}MPSVacSeq-TestSel","{V4}MPSVacSeq-TestSel",
					"{V5}MPSVacSeq-TestSel", "{V6}MPSVacSeq-TestSel", "{V7}MPSVacSeq-TestSel"};
assign VVS_SEQ_CMD_O to {"{V0}MPSVacSeq-CmdOpen","{V1}MPSVacSeq-CmdOpen","{V2}MPSVacSeq-CmdOpen","{V3}MPSVacSeq-CmdOpen","{V4}MPSVacSeq-CmdOpen",
					"{V5}MPSVacSeq-CmdOpen", "{V6}MPSVacSeq-CmdOpen", "{V7}MPSVacSeq-CmdOpen"};
assign VVS_SEQ_CMD_C to {"{V0}MPSVacSeq-CmdClose","{V1}MPSVacSeq-CmdClose","{V2}MPSVacSeq-CmdClose","{V3}MPSVacSeq-CmdClose","{V4}MPSVacSeq-CmdClose",
					"{V5}MPSVacSeq-CmdClose", "{V6}MPSVacSeq-CmdClose", "{V7}MPSVacSeq-CmdClose"};
assign VVS_SEQ_CMD_S_BP to {"{V0}MPSVacSeq-CmdSet","{V1}MPSVacSeq-CmdSet","{V2}MPSVacSeq-CmdSet","{V3}MPSVacSeq-CmdSet","{V4}MPSVacSeq-CmdSet",
					"{V5}MPSVacSeq-CmdSet", "{V6}MPSVacSeq-CmdSet", "{V7}MPSVacSeq-CmdSet"};
assign VVS_SEQ_CMD_R_BP to {"{V0}MPSVacSeq-CmdReset","{V1}MPSVacSeq-CmdReset","{V2}MPSVacSeq-CmdReset","{V3}MPSVacSeq-CmdReset","{V4}MPSVacSeq-CmdReset",
					"{V5}MPSVacSeq-CmdReset", "{V6}MPSVacSeq-CmdReset", "{V7}MPSVacSeq-CmdReset"};

monitor StartVVSTest;
monitor EndVVSTest;
monitor TimeEval;
monitor TimeStep;
monitor OpOpen;
monitor OpClose;
monitor OpSetVBP;
monitor OpResetVBP;
monitor VVS_Sw_C_En;
monitor VVS_Sw_C_Stat;
monitor VVS_O_En;
monitor VVS_C_En;
monitor VVS_OC_Err;
monitor VVS_BP_C_En;
monitor VVS_BP_C_Stat;
monitor VVS_BP_Err;
monitor VVS_BP_En;
monitor VVS_BP_Stat;
monitor VVS_OR;
monitor VVS_CR;
monitor VVS_SBP;
monitor VVS_RBP;
monitor VVS_SEL;

/*State Sets*/
/*State set for MPSVac position switches and VBPs hardware commissioning*/
ss VVS_Seq {
    state init_state {
	when ((StartVVSTest == TRUE) && delay(TimeStep) && (EndVVSTest == FALSE)) { //Start test when the start button is pressed and the test has not been stopped during TimeStep seconds
		InitState = TRUE; //OPI update
		pvPut(InitState);
		EndState = FALSE; //OPI update
		pvPut(EndState);
		TestGo = TRUE; //Test is on-going
		pvPut(TestGo);
		EvalTest = 0; //OPI update: Final Evaluation of the test is undefined
		pvPut(EvalTest);
		InformFBIS = TRUE; //FBIS informed about test on-going
		pvPut(InformFBIS);
		InformVac = TRUE; //Vacumm controls informed about test on-going
		pvPut(InformVac);
		InformMPSVac = TRUE; //MPSVac informed about test on-going
		pvPut(InformMPSVac);
	    printf("sncMPSVac: FBIS, Vac.Controls and MPSVac informed to enter Test mode\n");
		} state req_close_reset
	}

	state req_close_reset {
	when (delay(TimeStep) && (EndVVSTest == FALSE)){
		ReqCloseReset = TRUE; //OPI update
		pvPut(ReqCloseReset);
		InitState = FALSE; //OPI update
		pvPut(InitState);			
		printf("sncMPSVac: Request to Vac.Controls to CLOSE/RESET VBP of selected VVSs\n");
		for ( i = 0; i < N_VVS; i++ ){ //Valves selected are commanded to be closed and reset VBP
			 VVS_SEQ_CMD_O[i] = FALSE;//Remove the requests to Open and Set VBP for all the valves
		     VVS_SEQ_CMD_S_BP[i] = FALSE;			 	
		     pvPut(VVS_SEQ_CMD_O[i]);
		     pvPut(VVS_SEQ_CMD_S_BP[i]);
			 if (VVS_SEL[i]){ // Only selected Valves are requested to be closed (if operation is close) and reset VBP (if operation is reset VBP)
			 	if (OpClose) 
					VVS_SEQ_CMD_C[i] = TRUE;
				else
				 	VVS_SEQ_CMD_C[i] = FALSE;
				if (OpResetVBP) 
					VVS_SEQ_CMD_R_BP[i] = TRUE;				
				else
					VVS_SEQ_CMD_R_BP[i] = FALSE;				
			 }
			 else{ 
			 	VVS_SEQ_CMD_C[i] = FALSE;			 
				VVS_SEQ_CMD_R_BP[i] = FALSE;
			 }
			pvPut(VVS_SEQ_CMD_C[i]);
			pvPut(VVS_SEQ_CMD_R_BP[i]);
		}		
		} state eval_close_reset
	}

	state eval_close_reset {
	when (delay(TimeEval) && (EndVVSTest == FALSE)){
		EvalCloseReset = TRUE; //OPI update
		pvPut(EvalCloseReset);
		ReqCloseReset = FALSE; //OPI update
		pvPut(ReqCloseReset);		
		printf("sncMPSVac: Evaluating CLOSE VVS/RESET VBP of selected VVSs\n");
		for ( i = 0; i < N_VVS; i++ ){
			 //TEST cables for VVS position switches and VBP
			 VVS_CB_T[i] = 0; //Initially cable for position switches not tested
			 VVS_BP_CB_T[i] = 0; //Initially cable for VBP not tested
			 if (VVS_SEL[i]){ //If valve selected to test: Check Cables for Position switches and VBP
				 if (VVS_Sw_C_En[i] && VVS_Sw_C_Stat[i]) // cable connected from position swicthes to the valve and valid input read by the PLC
				 	VVS_CB_T[i] = 1; //Test cable connected for VVS OK				
				 else 
				 	VVS_CB_T[i] = 2; //Test cable connected for VVS FAILED
				 if (VVS_BP_C_En[i] && VVS_BP_C_Stat[i]) // cable connected from VBP to vac. controls and valid input read by the PLC
				 	VVS_BP_CB_T[i] = 1; //Test cable connected for VBP OK				
				 else 
				 	VVS_BP_CB_T[i] = 2;	//Test cable connected for VBP FAILED
			 }
			 pvPut(VVS_CB_T[i]);
			 pvPut(VVS_BP_CB_T[i]);	
			 //TEST positions switches for VVS Close
			 VVS_C_T[i] = 0;//Initially valve close switches are not tested
			 if (VVS_SEL[i] && OpClose && VVS_CR[i]){ //If valve selected to test and close operation is requested and VVS is Closed(confirmed by Vac.Controls): Check Close Position Switches for the VVS
				 if (VVS_C_En[i] && (VVS_OC_Err[i] == FALSE) && (VVS_CB_T[i]==1)) // valve closed detected and not error in swicthes read by PLC and cable ok
				 	VVS_C_T[i] = 1;	//Test position swicthes for VVS closed OK			
				 else 
				 	VVS_C_T[i] = 2;	//Test position swicthes for VVS closed FAILED
			 }
			 pvPut(VVS_C_T[i]);
			//TEST VBP signals for VBP Reset
			 VVS_BP_R_T[i] = 0;//Initially VBP reset is not tested
			 if (VVS_SEL[i] && OpResetVBP && VVS_RBP[i]){ //Valve selected to test and reset VBP operation is selected and VBP Reset from Vaccum controls: Check VBP signals for the VVS
			 	if ((VVS_BP_En[i] == FALSE) && VVS_BP_Stat[i] && (VVS_BP_Err[i] == FALSE) && VVS_BP_CB_T[i]) // VBP reset and channel status ok and no discrepancy error and cable ok
				 VVS_BP_R_T[i] = 1;	//Test reset VBP for VVS OK			
				else 
				 VVS_BP_R_T[i] = 2;	//Test reset VBP for VVS FAILED
			 }			 
			pvPut(VVS_BP_R_T[i]);
		}		
		} state req_open_set
	}

	state req_open_set {
	when (delay(TimeStep) && (EndVVSTest == FALSE)){
		ReqOpenSet = TRUE; //OPI update
		pvPut(ReqOpenSet);
		EvalCloseReset = FALSE; //OPI update
		pvPut(EvalCloseReset);	
		printf("sncMPSVac: Request to Vac.Controls to OPEN/SET VBP of selected VVSs\n");
		for ( i = 0; i < N_VVS; i++ ){ 
			 VVS_SEQ_CMD_C[i] = FALSE;//Remove the request to Close and Reset VBP for all the valves
			 VVS_SEQ_CMD_R_BP[i] = FALSE;
			 pvPut(VVS_SEQ_CMD_C[i]);
			 pvPut(VVS_SEQ_CMD_R_BP[i]);
			 if (VVS_SEL[i]){//Only selected valves are commanded to be opened (if operation is open) and set VBP (if operation is set VBP is chosen)
			 	if (OpOpen)
					VVS_SEQ_CMD_O[i] = TRUE;
				else 
					VVS_SEQ_CMD_O[i] = FALSE;
				if (OpSetVBP)
					VVS_SEQ_CMD_S_BP[i] = TRUE;	
				else	
					VVS_SEQ_CMD_S_BP[i] = FALSE;				
			 }
			 else{ 
			 	VVS_SEQ_CMD_O[i] = FALSE;			 
				VVS_SEQ_CMD_S_BP[i] = FALSE;
			 }
			pvPut(VVS_SEQ_CMD_O[i]);
			pvPut(VVS_SEQ_CMD_S_BP[i]);
		}		
		} state eval_open_set
	}

	state eval_open_set {
	when (delay(TimeEval) && (EndVVSTest == FALSE)){
		EvalOpenSet = TRUE; //OPI update
		pvPut(EvalOpenSet);
		ReqOpenSet = FALSE; //OPI update
		pvPut(ReqOpenSet);		
		printf("sncMPSVac: Evaluating OPEN VVS/SET VBP of selected VVSs\n");
		for ( i = 0; i < N_VVS; i++ ){
			 //TEST cables for VVS position switches and VBP
			 VVS_CB_T[i] = 0; //Initially cable for position switches not tested
			 VVS_BP_CB_T[i] = 0; //Initially cable for VBP not tested
			 if (VVS_SEL[i]){ //If valve selected to test: Check Cables for Position switches and VBP
				 if (VVS_Sw_C_En[i] && VVS_Sw_C_Stat[i]) // cable connected from position swicthes to the valve and valid input read by the PLC
				 	VVS_CB_T[i] = 1; //Test cable connected for VVS OK				
				 else 
				 	VVS_CB_T[i] = 2; //Test cable connected for VVS FAILED
				 if (VVS_BP_C_En[i] && VVS_BP_C_Stat[i]) // cable connected from VBP to vac. controls and valid input read by the PLC
				 	VVS_BP_CB_T[i] = 1; //Test cable connected for VBP OK				
				 else 
				 	VVS_BP_CB_T[i] = 2;	//Test cable connected for VBP FAILED
			 }
			 pvPut(VVS_CB_T[i]);
			 pvPut(VVS_BP_CB_T[i]);			 
			 //TEST positions switches for VVS Opened
			 VVS_O_T[i] = 0;//Initially valve open switches are not tested
			 if (VVS_SEL[i] && OpOpen && VVS_OR[i]){ //If valve selected to test and open operation is requested and Opened: Check Open Position Switches for the VVS
				 if (VVS_O_En[i] && (VVS_OC_Err[i] == FALSE) && (VVS_CB_T[i]==1)) // valve opened detected and not error in switches read by PLC and cable Ok
				 	VVS_O_T[i] = 1;	//Test position swicthes for VVS opened OK			
				 else 
				 	VVS_O_T[i] = 2;	//Test position swicthes for VVS opened FAILED
			 }
			 pvPut(VVS_O_T[i]);
			//TEST VBP signals for VBP Set
			 VVS_BP_S_T[i] = 0;//Initially VBP set is not tested
			 if (VVS_SEL[i] && OpSetVBP && VVS_SBP[i]){ //Valve selected to test and set VBP operation is requested and VBP Set: Check VBP signals for the VVS
			 	if (VVS_BP_En[i] && VVS_BP_Stat[i] && (VVS_BP_Err[i] == FALSE) && VVS_BP_CB_T[i]) // VBP set and channel status ok and no discrepancy error and cable ok
				 VVS_BP_S_T[i] = 1;	//Test VBP Set ok			
				else 
				 VVS_BP_S_T[i] = 2;	//Test VBP set FAILED
			 }			 
			pvPut(VVS_BP_S_T[i]);
		}		
		} state end_test
	}

    state end_test {
	when (delay(TimeStep) && (EndVVSTest == FALSE)) {
		EndState = TRUE; // OPI update
		pvPut(EndState);
		EvalOpenSet = FALSE; //OPI update
		pvPut(EvalOpenSet);
		TestGo = FALSE; //Test ended
		pvPut(TestGo);			
		InformFBIS = FALSE;// Informing FBIS, Vac.controls and MPSVac
		pvPut(InformFBIS);
		InformVac = FALSE;
		pvPut(InformVac);
		InformMPSVac = FALSE;
		pvPut(InformMPSVac);
		//Remove the requests to vac controls
		for ( i = 0; i < N_VVS; i++ ){ 
			 VVS_SEQ_CMD_C[i] = FALSE;//Remove the request to Close and Reset VBP for all the valves
			 VVS_SEQ_CMD_R_BP[i] = FALSE;
			 pvPut(VVS_SEQ_CMD_C[i]);
			 pvPut(VVS_SEQ_CMD_R_BP[i]);
			 VVS_SEQ_CMD_O[i] = FALSE;//Remove the request to Open and Set VBP for all valves
			 VVS_SEQ_CMD_S_BP[i] = FALSE;							 
			 pvPut(VVS_SEQ_CMD_O[i]);
			 pvPut(VVS_SEQ_CMD_S_BP[i]);
		}	
		//Evaluate final result of the test: UNDEFINED(0), PASSED(1), FAILED(2), INCOMPLETE(3)
		EvalTest = 0; //Initially set it as UNDEFINED
		EvalPassed = TRUE;
		EvalFailed = FALSE;		
		for ( i = 0; i < N_VVS; i++ ){
			if (VVS_SEL[i]){
				EvalPassed = EvalPassed && (VVS_CB_T[i] == 1) && (VVS_BP_CB_T[i] == 1) && (VVS_O_T[i] == 1) && (VVS_C_T[i] == 1) && (VVS_BP_S_T[i] == 1) && (VVS_BP_R_T[i] == 1);
				EvalFailed = EvalFailed || (VVS_CB_T[i] == 2) || (VVS_BP_CB_T[i] == 2) || (VVS_O_T[i] == 2) || (VVS_C_T[i] == 2) || (VVS_BP_S_T[i] == 2) || (VVS_BP_R_T[i] == 2);				
			}
		}
		if (EvalPassed) //Test is PASSED
			EvalTest = 1;
		if (EvalFailed) //Test is FAILED
			EvalTest = 2;
		if ((EvalPassed == FALSE) && (EvalFailed == FALSE)) //Test is incomplete
			EvalTest = 3; 
		pvPut(EvalTest);
	    printf("sncMPSVac: Test Ended\n");
		} state init_state
    }    
}
